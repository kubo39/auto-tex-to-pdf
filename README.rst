auto-tex-to-pdf
================


ファイル/ディレクトリの変更を監視して、変更されたファイルが.texだったら
tex -> dvi -> pdfに変換.

監視してるディレクトリと同じディレクトリで作業すると毎回platexとかがはしるので
編集したファイルをcpする.

prerequirements
-----------------

- Python 2.x >= 6

- pyinotify

- sh


3rdパーティーのライブラリをまとめてインストールしたかったら

- pip install -r requirements.txt
