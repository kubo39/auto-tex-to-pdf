from pyinotify import WatchManager, Notifier, IN_MODIFY, ProcessEvent
import sh


wm = WatchManager()
mask = IN_MODIFY

nkf = sh.Command("/usr/bin/nkf")
platex = sh.Command("/usr/bin/platex")
dvipdfmx = sh.Command("/usr/bin/dvipdfmx")


class PTmp(ProcessEvent):

    def process_IN_MODIFY(self, event):
        fname = event.name
        to_pdf = ''.join(fname.split('.')[:-1])
        ext = fname.split('.')[-1]
        if ext == 'tex':
            print fname
            nkf("-e", "--overwrite", fname)
            platex(fname)
            print platex(fname)
            print dvipdfmx("%s.dvi" % to_pdf)


if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        file = sys.argv[1]
    else:
        file = './'

    notifier = Notifier(wm, PTmp())
    wdd = wm.add_watch(file, mask, rec=True)

    notifier.loop()
