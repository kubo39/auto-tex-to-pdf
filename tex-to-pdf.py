import sys
import sh

nkf = sh.Command("/usr/bin/nkf")
platex = sh.Command("/usr/bin/platex")
dvipdfmx = sh.Command("/usr/bin/dvipdfmx")

if len(sys.argv) < 2:
    print "No file."
    sys.exit(1)


fname = sys.argv[1]

to_pdf = ''.join(fname.split('.')[:-1])
ext = fname.split('.')[-1]
if ext == 'tex':
    print fname
    nkf("-e", "--overwrite", fname)
    platex(fname)
    print platex(fname)
    print dvipdfmx("%s.dvi" % to_pdf)
    
